const Bluebird = require('bluebird');
const Mongoose = require('mongoose');
// enable promises on all mongoose actions
Bluebird.promisifyAll(Mongoose);
Mongoose.Promise = Bluebird;

// a plugin for auto inc using mongoose
const autoInc = require('mongoose-sequence')(Mongoose);
const Logger = require('../helpers/logger.helper')(__filename);

// actually try to connect to the db
const connectString = process.env.MONGODB || 'mongodb://localhost:27017/303exercise';
const connection = Mongoose.connect(connectString, { useMongoClient: true })
  .then(() => {
    Logger.info('Mongo connection was successful');
  }).catch((err) => {
    Logger.error('There was an error connecting to mongo', err);
  });

// export a singleton module here so we only use a single connection pool.
module.exports = {
  connection,
  autoIncrement: autoInc,
};
