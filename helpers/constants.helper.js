module.exports = {
  ENV: process.env.NODE_ENV,
  API: {},
  LOGGLY: {
    TOKEN: process.env.LOGGLY_TOKEN || '',
    SUBDOMAIN: process.env.LOGGLY_SUBDOMAIN || '',
  },
  REDIS: {
    URL: process.env.REDIS_URL || '',
  },
};
