const Logger = require('../helpers/logger.helper')(__filename);
const RecordSchema = require('../schemas/record.schema.js');
/**
 * This controller is responsible for reading and returning all records in the
 * records collection.
 * @module controllers/getAll
 */
module.exports = async function getAll(req, res) {
  return RecordSchema
    .find({ })
    .then((records) => {
      Logger.debug('Found all records successfully');
      res.status(200).json({ payload: records, success: true });
    })
    .catch((err) => {
      Logger.error('There was an error reading a record with the provided id', err);
      res.status(500).json({ message: err, success: false });
    });
};
