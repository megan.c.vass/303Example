// Dealing with mongo ids requires access _id in a fair amount of cases.
/* eslint no-underscore-dangle: 0 */
const Logger = require('../helpers/logger.helper')(__filename);
const RecordSchema = require('../schemas/record.schema.js');
/**
 * This controller is responsible for updating a single record with the provided
 * data. The data is re-validated on entry via the same model used to write.
 * @module controllers/deleteSingle
 */
module.exports = async function putSingleController(req, res) {
  // load the data into a model to validate it.
  const recordModel = new RecordSchema(req.body);
  // This raw object is later assigned as the scrubbed output of the model.
  let rawObject = {};
  /*
  * run a sync validation because if the data is bad we don't want to continue
  * processing.
  */
  const validationErrors = recordModel.validateSync();

  // check for validation issues on the data and return an error if there is any.
  if (validationErrors) {
    Logger.error('Invalid data for POST request', validationErrors);
    return res.status(400).json({
      message: 'Invalid or bad data was provided',
      success: false,
    });
  }
  /*
  * The Mongoose models add fields that we don't need. So we export just the
  * Object version of our now validated data object.
  */
  rawObject = recordModel.toObject();
  // grab our id out of the raw data now.
  const { id } = rawObject;
  // You can't update the _id field once it is written, so we remove it if it exists.
  if (rawObject._id) { delete rawObject._id; }
  // Since we are not using a protected ID we remove it so it can't be overwritten.
  if (rawObject.id) { delete rawObject.id; }
  // actually write our record update
  return RecordSchema
    .update({ id }, rawObject)
    .then((record) => {
      res.status(200).json({ payload: record, success: true });
    })
    .catch((err) => {
      Logger.error('There was an error updating a record', err);
      return res.status(500).json({ message: err, success: false });
    });
};
