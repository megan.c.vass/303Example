const Logger = require('../helpers/logger.helper')(__filename);
const RecordSchema = require('../schemas/record.schema.js');
/**
 * This controller is responsible for reading and returning a single record
 * based on the id passed as a GET param within the call URL.
 * @module controllers/deleteSingle
 */
module.exports = async function getSingleController(req, res) {
  // Make sure the controller was even passed something.
  if (!req.params.id) {
    Logger.error('Invalid data for id provided for delete');
    return res.status(400).json({
      message: 'Invalid or bad data was provided',
      success: false,
    });
  }

  // we can't use a string that doesn't convert to a number for look ups.
  let id;
  try {
    id = parseInt(req.params.id, 10);
  } catch (err) {
    Logger.error(
      `Invalid lookup id provided. Found value ${req.params.id} but expected to be castable to Number`,
      err,
    );
    return res.status(500).json({ message: err, success: false });
  }

  // Actually query for the record and return it if it was found.
  return RecordSchema
    .findOne({ id })
    .then((record) => {
      res.status(200).json({ payload: record, success: true });
    })
    .catch((err) => {
      Logger.error('There was an error reading a record with the provided id', err);
      return res.status(500).json({ message: err, success: false });
    });
};
