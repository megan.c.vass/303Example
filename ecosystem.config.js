module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [
    {
      name: 'core-service',
      script: 'index.js',
      log_date_format: 'YYYY-MM-DD HH:mm Z',
      merge_logs: true,
      next_gen_js: true,
      max_restarts: 2,
      min_uptime: '10s',
      instances: 1,
      node_args: ['--harmony'],
      error_file: './logs/server-err.log',
      out_file: './logs/server-out.log',
      pid_file: './logs/server-process.pid',
      ignore_watch: [
        'logs/**.*',
        '.git/**.*',
        'node_modules/**.*',
        'spec/**.*',
      ],
      env: {
        PORT: 8080,
      },
      env_dev: {
        NODE_ENV: 'dev',
        MONGODB: 'mongodb://localhost:27017/303exercise',
      },
      env_qa: {
        NODE_ENV: 'qa',
        MONGODB: '',
      },
      env_prod: {
        NODE_ENV: 'production',
        MONGODB: '',
      },
    },
  ],
};
