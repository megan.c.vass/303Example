require('pmx').init({
  http: true,
  errors: true,
  custom_probes: true,
  network: true,
  ports: true,
  transactions: true,
});
require('./helpers/mongo.helper.js');
const express = require('express');

const App = express();
const helmet = require('helmet');
const bodyParser = require('body-parser');
const compression = require('compression');
const Logger = require('./helpers/logger.helper')('server.js');

// Inject the security module(s)
App.use(helmet());

// Set gzip on all assets.
App.use(compression());

// setup our JSON parser for potential larger pay loads.
App.use(bodyParser.json({ limit: '5mb' }));
App.use(bodyParser.urlencoded({ limit: '5mb', extended: true }));

// =================== API Route Registrations ===================
App.get('/health', require('./controllers/health.api'));

App.get('/record/:id', require('./controllers/getSingle.controller'));
App.post('/record', require('./controllers/postSingle.controller'));
App.get('/record', require('./controllers/getAll.controller'));
App.delete('/record/:id', require('./controllers/deleteSingle.controller'));
App.put('/record/', require('./controllers/putSingle.controller'));

let Server;
const envPort = process.env.PORT || 8080;
const ServerStartCallback = function serverCallback() {
  const { port } = Server.address();
  Logger.info('Running server on port %s in %s mode', port, process.env.NODE_ENV);
};
Server = App.listen(envPort, ServerStartCallback);
module.exports = App;
// You can change the server timeout for http requests going OUT with this
// Server.timeout = 200000;
