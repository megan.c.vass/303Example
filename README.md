# 303 Software Service Exercise

## Introduction

> A simple web RESTful service that provides the ability to manipulate a single collection within mongo via a stateless API.

## Installation

> This project requires Node.js 8.9.1+ and npm 5.5.1+. To install the required dependencies simply clone the repo and run ```npm i```, let it run it's course.

> You will require Mongo DB to be running on a system
that the machine that the service is running on can access via local connection or over a network. Where
the service connects to can be controlled via environment variable found in the ecosystem.config.js file.
You can also use a normally ```SET``` environment variable and PM2 the process runner will use that over
the definition in the ecosystem file. By default if there is no setting provided it will attempt to
connect to ```mongodb://localhost:27017/```.

> *NOTE ABOUT DEV DEPENDENCIES*: If you don't have your development environment setup with a globally set environment variable that npm uses called ```NODE_ENV``` and the value to ```dev``` or ```development``` then the dependencies for the seed tool won't be installed. If you don't want to set that you will need to run ```npm i --only=dev``` within the project directory and the Casual library that is used to generate seed data will be installed. This isn't included as a normal dependency as it is not required for normal operation of the service.


# Available Commands
> The available commands for this project are run through npm as a run tool. The following commands are available ```npm start```, ```npm run start:dev```, ```npm run lint```, and ```npm run seed``` further details on these commands can be found below.

## ```npm start```
> This command will start the service in production mode. This can be noted by the ```--env prod``` flag seen in the package.json command definition. That flag is accessible in the service via ```process.env.NODE_ENV```.

## ```npm run start:dev```
> This command functions just like ```npm start``` accept it enables code base change watching and the flag value of ```process.env.NODE_ENV``` is set to 'dev'.

## ```npm run lint```
> This command will run through all of the relevant code directories and entry point file and detect formatting and potential syntax issues using Air BnB's base eslint profile.

## ```npm run dev:panel```
> This command will bring up the PM2 process panel so you can see real time straming of logs, process stats, and manage multiple instances at once on your dev box.

## ```npm run seed```
> This command will seed the localhost's mongo instance with a new collection if there isn't one present and then precede to insert 4 random records into it that can be used as example data.

>You can manually run this command with the command line arg as well see the following example:
```node seed.tool.js numberOfRecords=10```
You can define the entire mongodb string that it will connect to including the database via a uri.
It will look for the same env var as the service does ```MONGODB```. You can set that as a env var
either before the command or as a profile value in something such as .bashrc.

# Available APIs
> The following end points are available to you when the service is running, all API endpoints use JSON as a common transport standard. All API responses are returned using the following standard:

> Errors will be presented with an object with a message and success property which will be false, i.e ```{ message: 'error message', success: false }``` In addition to that the status code will be either a 500 for 400 error informing you if it is data related or service error related.

> Successes will be presented with an object that contains a payload and success property which will be true. The payload property will contain what ever data you have requested. i.e ```{ payload: { title: '...', author: '...', id : Number, content: '...'}, success: true }```

## GET /record/
> This end point will return all of the current records that have been written within an array that will be assigned to the payload property if successful.

## GET /record/:id
> This end point will return a specific record assuming that one exists. If the record exists it will return the single record under the payload property. If no record exists payload will simply be ```null```.

## POST /record/
> This end point will create a new record with the data provided assuming the required fields are met.
If the record is successfully created it will return the new record as the payload property.

## DELETE /record/:id
> This end point will delete a single record assuming that one with the provided id exists. If the record does exist then the payload property will return the mongo stat code for the action ```{ n: Number, ok: Number }``` the n property represents how many records were removed. If n is 0 then the record didn't exist to delete.

## PUT /record/
> This end point will update a record with the given properties assuming that you have passed valid fields with valid values and the record exists. The payload response will always return the mongodb stat object about the operation, in the case of an update it looks like the following ```{ n: Number, nModified: Number, ok: Number }```. The n as in the DELETE API represents the number of records inserted ( this will always be 0 for PUT ), nModified is the number of records updated, and ok ( 0,1 ) shows if the operation ran into any issues.

> If no id exists that you passed the API via the id property on the object passed as the request body. then nModified will be 0.

# Technology Overview
> There are various pieces of technology used within this service below is a list of the key ones and what they are used for.

## Express
> Express is a HTTP server and routing utility for Node.js that allows for simple http request interception and manipulation via the middleware pattern.

## PM2/PMX
> PM2 is a 'always up' process manager and runner for Node.js ( and a few other languages ) that enables easier management of application and service lifecycles through environments in addition to being used to scale process across a single instance or machine via clustering. PMX is PM2's metrics end point agent that allows metrics to be collected through the service KeyMetrics or via a JSON end point.

## Mongo DB
> Mongo DB is a scalable document store database used for slow mutative to none mutative data that requires some but not strong relational ties.

## Mongoose
> Mongoose is an ODM layer over the Mongo store technology that enables schema definitions that power writing and validating data before and after any interactions with Mongo.

## Casual
> Casual is a small library for generating arbitrary strings. It is a great tool for seeding and fake data generation.

## Bluebird
> Bluebird is a stand alone Promise library that still boasts the fastest implementation of the Promise paradigm at the time. It is used to wrap Mongoose actions so Promises can be used in a transactional way.

## Helmet
> Helmet is a security package that simply patches the most common security issues that revolve around most HTTP servers these days.
